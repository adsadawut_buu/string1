/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.string1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
    
    
}
/**
 *
 * @author อัษฎาวุฒิ
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500,300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your Name: ");
        lblYourName.setSize(70,20);
        lblYourName.setLocation(100, 5);
        lblYourName.setBackground(Color.WHITE);
        lblYourName.setOpaque(true);
        
        JButton btnHello = new JButton("Helllo");
        btnHello.setSize(70,20);
        btnHello.setLocation(175, 30);
        
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());
        //Anonymous Class
        ActionListener actionListener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class: Action");
            }
        };
        btnHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello...",JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(175, 100);
        lblHello.setBackground(Color.WHITE);
        lblHello.setOpaque(true);
        
        
        JTextField txtHello = new JTextField();
        txtHello.setSize(200,20);
        txtHello.setLocation(175, 5);
        
        
        
        
        frmMain.setLayout(null);
        
        frmMain.add(lblYourName);
        frmMain.add(txtHello);
        frmMain.add(btnHello);
        frmMain.add(lblHello);
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    String name = txtHello.getText();
                    lblHello.setText("Hello "+name);
            }
        });
        frmMain.setVisible(true);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe: Action");
    }
}
